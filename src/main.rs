use std::{collections::HashMap, env, fs, io::{Read, Write}, net::{TcpListener, TcpStream}, path::Path};
use itertools::Itertools;

#[derive(Debug, Clone)]
pub struct RawRequest<'a> {
    request_lines: Vec<&'a str>,
    request_body: String,
    request_string: String,
    headers: HashMap<String, String>,
    path: String,
    method: String,  
}

impl<'a> RawRequest<'a> {
  pub fn new(mut stream: &TcpStream) -> Self {
    let mut buffer = [0; 512];
    let bytes_read = stream.read(&mut buffer).unwrap();

    let mut request_string = String::new();
    if bytes_read > 0 {
        request_string = String::from_utf8(Vec::from(buffer)).unwrap();
    }
    RawRequest {
        request_string,
        headers: HashMap::new(),
        path: String::new(),
        method:  String::new(),
        request_lines: Vec::new(),
        request_body: String::new(),
    }
  }

  pub fn process_request(&'a mut self) -> RawRequest {
    self.request_lines = self.request_string.split("\r\n").collect_vec();
    let body = self.request_string.split("\r\n\r\n").collect_vec();
    if body.len() > 1 {
         self.request_body = body[1].to_string();
    }
  
    let method_path = self.request_lines.first().unwrap().to_owned().split(" ").collect_vec();

    self.method = method_path[0].to_owned();
    self.path = method_path[1].to_owned();

    let header = self.request_lines.iter().skip(1).collect_vec();
    for (_i, &item) in header.iter().enumerate() {
        let header_k_v = item.split(": ").collect_vec();    
        if  header_k_v.len() > 1 {
            self.headers.insert(header_k_v[0].to_owned(), header_k_v[1].to_owned());
        }
    }
    RawRequest {
        request_string: self.request_string.to_owned(),
        headers: self.headers.to_owned(),
        path: self.path.to_owned(),
        method: self.method.to_owned(),
        request_lines: self.request_lines.to_owned(),
        request_body: self.request_body.to_owned()
    }
  }
}

#[tokio::main]
async fn main() {
    // You can use print statements as follows for debugging, they'll be visible when running tests.
    println!("Logs from your program will appear here!");

    let listener = TcpListener::bind("127.0.0.1:4221").unwrap();
    for stream in listener.incoming() {
         match stream {
             Ok(stream) => {
                 println!("accepted new connection");
                 tokio::spawn(async move {
                    handle_connection(stream).await;
                });
             }
             Err(e) => {
                 println!("error: {}", e);
             }
         }
    }
}

pub async fn handle_connection(mut stream: TcpStream) {
    let mut raw_request = RawRequest::new(&stream);
    let request = raw_request.process_request();
    process_buffer(request, &mut stream);
}


pub fn process_buffer(request: RawRequest, stream:  &mut TcpStream) {
    if request.path == "/" {
        let _ = &stream.write_all("HTTP/1.1 200 OK\r\n\r\n".as_bytes()).unwrap();
    } else if request.path.starts_with("/echo/") {
        let path = &request.path[6..];
        let response = format!("HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nContent-Length: {}\r\n\r\n{}",path.len(), path);
        let _ = &stream.write_all( response.as_bytes());
    } else if request.path == "/user-agent" {
        let user_ag = request.headers.get("User-Agent").unwrap();
        let response = format!("HTTP/1.1 200 OK\r\nContent-Type: text/plain\r\nContent-Length: {}\r\n\r\n{}",user_ag.len(), user_ag);
        let _ = &stream.write_all( response.as_bytes());
    } else if request.path.starts_with("/files/") { 
        let args: Vec<String> = env::args().collect();
        let file = &request.path[7..];
        let file_path = Path::new(args[2].as_str()).join(file);
        if request.method == "GET" {
            let mut file_data = String::new();
            if file_path.exists() {
                match fs::read_to_string(&file_path) {
                    Ok(data) => {
                        file_data = data;
                    }
                    Err(e) => {
                        println!("Failed to read file: {}", e);
                    }
                }
                let response = format!( "HTTP/1.1 200 OK\r\nContent-Type: application/octet-stream\r\nContent-Length: {}\r\n\r\n{}\r\n", file_data.len(), file_data);
                let _ = &stream.write_all( response.as_bytes());
            } else {
                let _ = &stream.write_all("HTTP/1.1 404 Not Found\r\n\r\n".as_bytes()).unwrap();
            }
        }
        if request.method == "POST" {
                fs::write(file_path, request.request_body.replace("\x00", "")).unwrap();
                let response = format!("HTTP/1.1 201 OK\r\n\r\n");
                let _ = &stream.write_all( response.as_bytes());
        }
    }
    else {
        let _ = &stream.write_all("HTTP/1.1 404 Not Found\r\n\r\n".as_bytes()).unwrap();
    };
}


    
